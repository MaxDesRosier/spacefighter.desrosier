
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0) 
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		//Gets the time that the targeted enemy ship has been active. 
		m_activationSeconds += pGameTime->GetTimeElapsed();
		//Deactivates the enemy ship if it has been active and is not on screen for more than 2 seconds.
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	//Sets Enemy ships position.
	SetPosition(position);
	//Creates a delay timer for enemy ships spawning.
	m_delaySeconds = delaySeconds;
	// Creates Enemy Ship.
	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}